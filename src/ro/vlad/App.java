package ro.vlad;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

/**
 * Application for stamping a given text on a PDF as watermark
 * @author vlad.dima
 *
 */
public class App {

	private static final int WATERMARK_IMAGE_WIDTH = 490;
	private static final int WATERMARK_IMAGE_HEIGHT = 750;
	private static final float WATERMARK_IMAGE_POSITION_X = 50F;
	private static final float WATERMARK_IMAGE_POSITION_Y = 120F;
	private static final int WATERMARK_FONT_SIZE = 150;
	
	/**
	 * @param args - filePath, text 
	 */
	public static void main(String[] args) {

		//Proceed with the stamping only if the arguments are correct
		if (args.length != 2) {
			System.err.println("Please provide the following input parameters: filePath, text");
			System.err.println("For example: java -jar PdfStamper.jar pathToFile.pdf \"My text\"");
		} else {
			try {
				
				//Get the input parameters
				String filePath = args[0]; 
				String text = args[1];
				
				//Create an image from the provided text
//				Image image = Image.getInstance(App.createImageFromText(text, 20), null);
//				image.setAbsolutePosition(fpaddingLeft, fPaddingBottom);
				
				Image image = Image.getInstance(App.createWatermarkImage(text), null);
				image.setAbsolutePosition(WATERMARK_IMAGE_POSITION_X, WATERMARK_IMAGE_POSITION_Y);
				
				//Stamp the image on the PDF
				PdfReader pdfReader = new PdfReader(filePath);
				File temporayFile = new File("temp-file-for-pdfStamper.pdf");
				OutputStream outputStream = new FileOutputStream(temporayFile);
				PdfStamper stamper = new PdfStamper(pdfReader, outputStream);
				
				/** Stamp only the first page of the PDF **/
//				PdfContentByte pdfContentByte = stamper.getUnderContent(1);
//			    pdfContentByte.addImage(image);
//			    stamper.close();
			    
				/** Stamping all pages of the PDF **/
				PdfContentByte pdfContentByte = null;
				int total = pdfReader.getNumberOfPages() + 1;
				  for(int i = 1; i < total; i++) {	    
				    pdfContentByte = stamper.getUnderContent(i);
				    pdfContentByte.addImage(image);
				  }
				stamper.close();
				outputStream.close();
				
				//Delete the original file and rename the temporary file to the original file name
				File originalFile = new File(filePath);
				originalFile.delete();				
				temporayFile.renameTo(new File(filePath));
	
			} catch(Exception e) {
				System.err.println(e.getMessage());
			}
		}

	}

	/**
	* Converts a String to a java.awt.Image 
	*/
	private static java.awt.Image createImageFromText(String text, int fontSize) throws FileNotFoundException, FontFormatException, IOException {
	 
		//Because font metrics is based on a graphics context, we need to create
		//a small, temporary image so we can ascertain the width and height of the final image*/	   
		BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = img.createGraphics();
		Font font = new Font("Arial", Font.BOLD, fontSize);
		g2d.setFont(font);
		FontMetrics fm = g2d.getFontMetrics();
		int width = fm.stringWidth(text);
		int height = fm.getHeight();
		g2d.dispose();
		
		//Now create the needed image
		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		g2d = img.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		g2d.setFont(font);
		fm = g2d.getFontMetrics();
		g2d.setColor(Color.BLACK);
		g2d.drawString(text, 0, fm.getAscent());
		g2d.dispose();
		
		return img;
	}

   /**
    * Creates a watermark for the background of the PDF from the provided text
    * @param watermarkText
    * @return
    * @throws FileNotFoundException
    * @throws FontFormatException
    * @throws IOException
    */
   private static java.awt.Image createWatermarkImage(String watermarkText) throws FileNotFoundException, FontFormatException, IOException {
	
		// Create the transparent background
		int red = Color.WHITE.getRed();
		int green = Color.WHITE.getGreen();
		int blue = Color.WHITE.getBlue();
		int alpha = 0;
		int colorMask = (alpha << 24) | (red << 16) | (green << 8) | blue;
		Color backgroundColor = new Color(colorMask, true);
		Color foregroundColor = new Color(215, 215, 215);
		
		int imageWidth = WATERMARK_IMAGE_WIDTH;
		int imageHeight = WATERMARK_IMAGE_HEIGHT;
				
		Font font = new Font("Arial", Font.BOLD, WATERMARK_FONT_SIZE);
		
		// Create an initial image in order to get an Graphics2D instance
		// that will be used for calculating the font height to be used in order
		// to have the biggest font for the given watermark text and watermark image
		BufferedImage bufferedImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bufferedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		// Calculate the size of the text with the initial font size
		FontRenderContext fc = g2d.getFontRenderContext();
		Rectangle2D bounds = font.getStringBounds(watermarkText, fc);
		double width = bounds.getWidth();
		double height = bounds.getHeight();

		// Adjust the font size so that the text doesn't exceed the image size
		double x = height / Math.sqrt(2.0);
		double y = Math.sqrt(Math.pow(imageHeight - x, 2) + Math.pow(imageWidth - x, 2));
		int fontSize = WATERMARK_FONT_SIZE;
		while ((width > y) && (fontSize > 10)) {
			fontSize -= 2;
			font = font.deriveFont(fontSize);
			bounds = font.getStringBounds(watermarkText, fc);
			width = bounds.getWidth();
			height = bounds.getHeight();
			x = height / Math.sqrt(2.0);
			y = Math.sqrt(Math.pow(imageHeight - x, 2) + Math.pow(imageWidth - x, 2));
		}
		g2d.dispose();
		
		// Create the watermark image
		bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
		g2d = bufferedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setFont(font);
		
		// Configure the image with the transparent background and the foreground color 
		g2d.setColor(backgroundColor);
		g2d.fillRect(0, 0, imageWidth, imageHeight);
		g2d.setColor(foregroundColor);
		
		// Translate and rotate so that the watermark is written on the image
		// diagonal starting from the lower left corner to the upper right corner
		g2d.translate(imageWidth / 2.0f, imageHeight / 2.0f);
		AffineTransform at2 = new AffineTransform();
		double opad = imageHeight / ((double) imageWidth + 250);
		double angle = Math.toDegrees(Math.atan(opad));
		double idegrees = -1 * angle;
		double theta = (2 * Math.PI * idegrees) / 360;
		at2.rotate(theta);
		g2d.transform(at2);
		float x1 = (int) width / 2.0f * (-1);
		float y1 = (int) height / 2.0f;
		g2d.translate(x1, y1);

		// Draw the watermark text on the diagonal (lower left corner to upper right corner)
		g2d.drawString(watermarkText, 0.0f, 0.0f);

		// Free the graphic resources
		g2d.dispose();
		
		return bufferedImage;
   }
   
}
